from django.shortcuts import render

from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.contrib.auth import login
from django.views.generic.edit import CreateView

# from django.contrib.auth.models import User
# from django.contrib.auth import authenticate, login

# # Create your views here.
# def sign_up(request):
#     if request.method == "POST":
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             user = form.save()
#             login(request, user=user)
#             return redirect("home")


class SignUpView(CreateView):
    form_class = UserCreationForm
    template_name = "registration/signup.html"

    def form_valid(self, form):
        user = form.save()
        login(self.request, user=user)
        return redirect("home")
